﻿using Microsoft.AspNetCore.Mvc;
using MyDegree.Controllers;
using Xunit;


namespace MyDegree.Tests.Controllers
{

    /// <summary>
    /// HomeControllerTest provides unit tests for HomeController 
    /// and its traditional action methods returning an IActionResult result, 
    /// often a ViewResult. 
    /// </summary>
    public class HomeControllerTest
    {
        [Fact]
        public void Index_ReturnsAViewResult()
        {
            // Arrange
            var controller = new HomeController();

            // Act
            var result = controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);

        }

        [Fact]
        public void About_ReturnsAViewResult()
        {
            // Arrange
            var controller = new HomeController();

            // Act
            var result = controller.About();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);

        }

        [Fact]
        public void Contact_ReturnsAViewResult()
        {
            // Arrange
            var controller = new HomeController();

            // Act
            var result = controller.Contact();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);

        }

    }
}
