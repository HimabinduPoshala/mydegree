﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MyDegree.Data;
using MyDegree.Models;
using MyDegree.Services;

namespace MyDegree
{

    /// <summary>
    /// Startup class to configure our application.
    /// </summary>
    public class Startup {
    public Startup (IConfiguration configuration) {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices (IServiceCollection services) {
      services.AddDbContext<ApplicationDbContext> (options =>
        options.UseSqlServer (Configuration.GetConnectionString ("DefaultConnection")));

      services.AddIdentity<ApplicationUser, IdentityRole> ()
        .AddEntityFrameworkStores<ApplicationDbContext> ()
        .AddDefaultTokenProviders ();

      // Add application services.
      services.AddTransient<IEmailSender, EmailSender> ();
      services.AddTransient<DbInitializer> ();
      services.AddMvc ();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure (IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {
      if (env.IsDevelopment ()) {
        app.UseDeveloperExceptionPage ();
        app.UseBrowserLink ();
        app.UseDatabaseErrorPage ();
      } else {
        app.UseExceptionHandler ("/Home/Error");
      }
            loggerFactory.AddConsole(Configuration.GetSection("Logging")); //log levels set in appsettings Logging section
                                                                           //call ConfigureLogger in a centralized place
            ProgramLogger.ConfigureLogger (loggerFactory);
      //use this as the primary LoggerFactory available everywhere
      ProgramLogger.LoggerFactory = loggerFactory;

      app.UseStaticFiles ();

      app.UseAuthentication ();

      app.UseMvc (routes => {
        routes.MapRoute (
          name: "default",
          template: "{controller=Home}/{action=Index}/{id?}");
      });
    }
  }
}