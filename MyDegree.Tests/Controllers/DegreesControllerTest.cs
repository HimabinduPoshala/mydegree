﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MyDegree.Models;
using Xunit;
using Moq;
using MyDegree.Controllers;
using MyDegree.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections.Generic;

namespace MyDegree.Tests.Controllers
{

    /// <summary>
    /// DegreesControllerTest provides unit tests for DegreesController 
    /// and its async action methods returning an Task<IActionResult> result, 
    /// often a ViewResult if successful or NotFoundResult if unsuccessful.

    /// </summary>
    public class DegreesControllerTest
    {
        /// <summary>
        /// Helper function that returns a test ApplicationDbContext context object with no data.
        /// </summary>
        /// <returns>ApplicationDbContext</returns>
        private ApplicationDbContext GetContext()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                              .UseInMemoryDatabase(Guid.NewGuid().ToString())
                              .Options;
            var context = new ApplicationDbContext(options);
            return context;
        }

        /// <summary>
        /// Helper function that returns a test ApplicationDbContext context object with data.
        /// </summary>
        /// <returns>ApplicationDbContext</returns>
        private ApplicationDbContext GetContextAndData()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                             .UseInMemoryDatabase(Guid.NewGuid().ToString())
                             .Options;
            var context = new ApplicationDbContext(options);
            IList<Degree> list = new List<Degree>();
            var one = new Mock<Degree>().Object;
            var two = new Mock<Degree>().Object;
            list.Add(one);
            list.Add(two);
            context.Degrees = CreateDbSetMock(list).Object;
            return context;
        }

        private static Mock<DbSet<T>> CreateDbSetMock<T>(IEnumerable<T> elements) where T : class
        {
            var elementsAsQueryable = elements.AsQueryable();
            var dbSetMock = new Mock<DbSet<T>>();

            dbSetMock.As<IQueryable<T>>().Setup(m => m.Provider).Returns(elementsAsQueryable.Provider);
            dbSetMock.As<IQueryable<T>>().Setup(m => m.Expression).Returns(elementsAsQueryable.Expression);
            dbSetMock.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(elementsAsQueryable.ElementType);
            dbSetMock.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(elementsAsQueryable.GetEnumerator());

            return dbSetMock;
        }

        [Fact]
        public void GetContext_HasNoData()
        {
            // Arrange
            using (var context = GetContext())
            {
                // Act
                var count =  context.Degrees.Count();

                // Assert
                Assert.Equal(0, count);
            }
        }

        [Fact]
        public void GetContextAndData_HasData()
        {
            // Arrange
            using (var context = GetContextAndData())
            {
                // Act
                var count = context.Degrees.Count();

                // Assert
                Assert.Equal(2, count);
            }
        }

        [Fact]
        public async Task Index_ShouldReturnDefaultView()
        {
            // Arrange
            using (var context = GetContext()) 
            using (var controller = new DegreesController(context))
            {
                string sortOrder = "";
                string searchString = "";

                // Act
                var result = await controller.Index(sortOrder, searchString) as ViewResult;

                // Assert
                Assert.NotNull(result);
                Assert.True(string.IsNullOrEmpty(result.ViewName) || result.ViewName == "Index");

            }
        }

        [Fact]
        public async Task Details_ShouldHaveNotFoundResultWithBadId()
        {
            // Arrange
            using (var context = GetContext())
            using (var controller = new DegreesController(context))
            {
                int? id = -22;

                // Act
                var actionResult = await controller.Details(id);

                // Assert
                Assert.IsType<NotFoundResult>(actionResult);
            }
        }

    }
}
