﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using MyDegree.Controllers;
using MyDegree.Data;
using MyDegree.Models;
using Xunit;


namespace MyDegree.Tests.Controllers
{

    /// <summary>
    /// StudentsControllerTest provides unit tests for StudentsController 
    /// and its async action methods returning an Task<IActionResult> result, 
    /// often a ViewResult if successful or NotFoundResult if unsuccessful.
    /// </summary>
    public class StudentsControllerTest
    {

        /// <summary>
        /// Helper function that returns a test ApplicationDbContext context object with no data.
        /// </summary>
        /// <returns>ApplicationDbContext</returns>
        private ApplicationDbContext GetContext()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                              .UseInMemoryDatabase(Guid.NewGuid().ToString())
                              .Options;
            var context = new ApplicationDbContext(options);
            return context;
        }

        /// <summary>
        /// Helper function that returns a test ApplicationDbContext context object with data.
        /// </summary>
        /// <returns>ApplicationDbContext</returns>
        private ApplicationDbContext GetContextAndData()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                              .UseInMemoryDatabase(Guid.NewGuid().ToString())
                              .Options;
            var context = new ApplicationDbContext(options);
            var one = new Mock<Student>().Object;
            var two = new Mock<Student>().Object;
            var list = new List<Student> { one, two }.AsQueryable();
            context.Students = CreateDbSetMock(list).Object;
            return context;
        }

        private static Mock<DbSet<T>> CreateDbSetMock<T>(IEnumerable<T> elements) where T : class
        {
            var elementsAsQueryable = elements.AsQueryable();
            var dbSetMock = new Mock<DbSet<T>>();
            dbSetMock.As<IQueryable<T>>().Setup(m => m.Provider).Returns(elementsAsQueryable.Provider);
            dbSetMock.As<IQueryable<T>>().Setup(m => m.Expression).Returns(elementsAsQueryable.Expression);
            dbSetMock.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(elementsAsQueryable.ElementType);
            dbSetMock.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(elementsAsQueryable.GetEnumerator());
            return dbSetMock;
        }

        [Fact]
        public void GetContext_HasNoData()
        {

            // Arrange
            using (var context = GetContext())
            {
                // Act
                var count = context.Students.Count();

                // Assert
                Assert.Equal(0, count);
            }

        }

        [Fact]
        public void GetContextAndData_HasData()
        {
            // Arrange
            using (var context = GetContextAndData())
            {
                // Act
                var count = context.Students.Count();

                // Assert
                 Assert.Equal(2, count);
            }
        }

        [Fact]
        public async Task Index_ShouldReturnDefaultView()
        {
            // Arrange
            using (var context = GetContext())
            using (var controller = new StudentsController(context))
            {
                string sortOrder = "";
                string searchString = "";

                // Act
                var viewResult = await controller.Index(sortOrder, searchString) as ViewResult;

                // Assert
                Assert.NotNull(viewResult);
                Assert.True(string.IsNullOrEmpty(viewResult.ViewName) || viewResult.ViewName == "Index");
            }
        }

        [Fact]
        public async Task Details_ShouldReturnNotFoundResultIfIdNotFound()
        {
            //Arrange
            using (var context = GetContext())
            using (var controller = new StudentsController(context))
            {
                int id = -22;

                // Act
                var actionResult = await controller.Details(id);

                // Assert
                Assert.IsType<NotFoundResult>(actionResult);
                Assert.NotNull(actionResult);
            }
        }

        [Fact]
        public async Task Details_ShouldHaveNotFoundResultWithNoId()
        {
            // Arrange
            using (var context = GetContextAndData())
            using (var controller = new StudentsController(context))
            {
                int? id = null;

                // Act
                var actionResult = await controller.Details(id);

                // Assert
                Assert.IsType<NotFoundResult>(actionResult);
            }
        }

        [Fact]
        public async Task Details_ShouldHaveNotFoundResultWithBadId()
        {
            // Arrange
            using (var context = GetContext())
            using (var controller = new StudentsController(context))
            {
                int? id = -22;

                // Act
                var actionResult = await controller.Details(id);

                // Assert
                Assert.IsType<NotFoundResult>(actionResult);
            }
        }

        [Fact]
        public async Task PostCreate_ReturnsToIndexViewWhenModelStateIsInvalid()
        {
            // Arrange
        
            using (var context = GetContextAndData())
            using (var controller = new StudentsController(context))
            {
                var model = new Student(){   FamilyName = "Jones"  };
                var validationResults = new List<ValidationResult>();
                var validationContext = new ValidationContext(model, null, null);
                var valid = Validator.TryValidateObject(model, validationContext, validationResults, true);

                // Act
                var actionResult = await controller.Create(model);

                // Assert
                Assert.False(valid);
                var redirectToActionResult = Assert.IsType<RedirectToActionResult>(actionResult);
                Assert.Equal("Index", redirectToActionResult.ActionName);
      
            }
        }

        [Fact]
        public async Task PostCreate_RedirectsToIndexWhenModelStateIsValid()
        {
            // Arrange
            using (var context = GetContextAndData())
            using (var controller = new StudentsController(context))
            {
                var model = new Student()
                {
                    StudentId = 99,
                    FamilyName = "Smith",
                    GivenName = "Bob"
                };
                var validationResults = new List<ValidationResult>();
                var validationContext = new ValidationContext(model, null, null);
                var valid = Validator.TryValidateObject(model, validationContext, validationResults, true);

                // Act
                var actionResult = await controller.Create(model);
                var viewResult = actionResult as ViewResult;

                // Assert
                Assert.True(valid);
                var redirectToActionResult = Assert.IsType<RedirectToActionResult>(actionResult);
                Assert.Equal("Index", redirectToActionResult.ActionName);
            }
        }

      
    }
}
